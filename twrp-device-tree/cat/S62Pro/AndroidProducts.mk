#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_S62Pro.mk

COMMON_LUNCH_CHOICES := \
    omni_S62Pro-user \
    omni_S62Pro-userdebug \
    omni_S62Pro-eng
