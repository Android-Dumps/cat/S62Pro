#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_S62Pro.mk

COMMON_LUNCH_CHOICES := \
    lineage_S62Pro-user \
    lineage_S62Pro-userdebug \
    lineage_S62Pro-eng
