#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from S62Pro device
$(call inherit-product, device/cat/S62Pro/device.mk)

PRODUCT_DEVICE := S62Pro
PRODUCT_NAME := lineage_S62Pro
PRODUCT_BRAND := Cat
PRODUCT_MODEL := S62 Pro
PRODUCT_MANUFACTURER := cat

PRODUCT_GMS_CLIENTID_BASE := android-bullitt

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="S62Pro-user 11 RKQ1.210406.002 1.015.00 release-keys"

BUILD_FINGERPRINT := Cat/S62Pro/S62Pro:11/RKQ1.210406.002/1.015.00:user/release-keys
